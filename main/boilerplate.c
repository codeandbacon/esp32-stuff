/*
let's blink something
*/
// #include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "sdkconfig.h"
#include "esp_log.h"
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "nvs.h"
#include "nvs_flash.h"


void basic_task(void *pvParameter)
{
    printf("basic task started\n");
    while(1) {
        vTaskDelay(100);
        printf("delay...\n");
    }

}


void app_main()
{
    // initialize flash
    nvs_flash_init();
    printf("nvs_flash_init\n");

    xTaskCreate(&basic_task, "basic_task", 2048, NULL, 5, NULL);

}
